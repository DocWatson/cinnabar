extern crate cinnabar_types;
use self::cinnabar_types::{PokemonMove, PokemonSpecies};

use std::collections::HashMap;

use super::super::battle::Battle;
use super::super::team;

pub const MAIN_MENU: [&'static str; 2] = ["Battle!", "Teams"];
pub const BATTLE_MENU: [&'static str; 4] = ["ATTACK", "ITEM", "POKEMON", "RUN"];
pub const TEAM_BUILDER_MENU: [&'static str; 4] = [
    "Build a new team",
    "Create a random team",
    "Load a team",
    "Exit",
];

pub struct ListState<I> {
    pub items: Vec<I>,
    pub selected: usize,
}

impl<I> ListState<I> {
    pub fn new(items: Vec<I>) -> ListState<I> {
        ListState { items, selected: 0 }
    }
    fn select_previous(&mut self) {
        if self.selected > 0 {
            self.selected -= 1;
        }
    }
    fn select_next(&mut self) {
        if self.selected < self.items.len() - 1 {
            self.selected += 1
        }
    }
}

pub struct App<'a> {
    pub title: &'a str,
    pub should_quit: bool,
    pub menu_choices: ListState<&'a str>,
    pub mode: Option<usize>,
    pub pokemon_data: Vec<PokemonSpecies>,
    pub move_data: HashMap<i32, PokemonMove>,
    pub battle: Option<Battle>,
}

impl<'a> App<'a> {
    pub fn new(
        title: &'a str,
        pokemon: Vec<PokemonSpecies>,
        moves: HashMap<i32, PokemonMove>,
    ) -> App<'a> {
        App {
            title,
            should_quit: false,
            menu_choices: ListState::new(MAIN_MENU.to_vec()),
            pokemon_data: pokemon,
            move_data: moves,
            mode: None,
            battle: None,
        }
    }

    pub fn on_up(&mut self) {
        self.menu_choices.select_previous();
    }

    pub fn on_down(&mut self) {
        self.menu_choices.select_next();
    }

    pub fn on_key(&mut self, c: char) {
        match c {
            'q' => {
                self.should_quit = true;
            }
            '\n' => {
                // TODO: handle this state better
                // probably a menu module that takes MODE and SELECTION
                self.mode = Some(self.menu_choices.selected);

                match self.menu_choices.selected {
                    0 => {
                        self.menu_choices = ListState::new(BATTLE_MENU.to_vec());
                        self.battle = Some(Battle::create_battle());
                    }
                    1 => {
                        self.menu_choices = ListState::new(TEAM_BUILDER_MENU.to_vec());
                        team::create_test_team();
                    }
                    _ => self.menu_choices = ListState::new(MAIN_MENU.to_vec()),
                }
            }
            _ => {}
        }
    }

    pub fn on_tick(&mut self) {
        // do nothing
    }
}
