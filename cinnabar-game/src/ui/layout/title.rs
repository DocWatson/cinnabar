use crate::ui::app::App;

use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, Paragraph, SelectableList, Text, Widget};
use tui::Frame;

pub fn draw<B>(f: &mut Frame<B>, app: &App, area: Rect)
where
    B: Backend,
{
    let chunks = Layout::default()
        .constraints([Constraint::Percentage(60), Constraint::Percentage(40)].as_ref())
        .direction(Direction::Vertical)
        .split(area);
    draw_title(f, chunks[0]);
    draw_menu(f, app, chunks[1]);
}

fn draw_menu<B>(f: &mut Frame<B>, app: &App, area: Rect)
where
    B: Backend,
{
    let constraints = vec![Constraint::Percentage(30), Constraint::Percentage(70)];
    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Horizontal)
        .split(area);
    {
        SelectableList::default()
            .block(Block::default().borders(Borders::ALL).title("Menu"))
            .items(&app.menu_choices.items)
            .select(Some(app.menu_choices.selected))
            .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
            .highlight_symbol(">")
            .render(f, chunks[0]);

        draw_instructions(f, chunks[1], app.menu_choices.selected);
    }
}

fn draw_title<B>(f: &mut Frame<B>, area: Rect)
where
    B: Backend,
{
    let text = [
        Text::styled("
        ___ _                   _                
       / __(_)_ __  _ __   __ _| |__   __ _ _ __ 
      / /  | | '_ \\| '_ \\ / _` | '_ \\ / _` | '__|
     / /___| | | | | | | | (_| | |_) | (_| | |   
     \\____/|_|_| |_|_| |_|\\__,_|_.__/ \\__,_|_|                                                      
     ", Style::default().fg(Color::Red)),
    ];
    Paragraph::new(text.iter())
        .block(Block::default().borders(Borders::ALL))
        .render(f, area);
}

fn draw_instructions<B>(f: &mut Frame<B>, area: Rect, choice: usize)
where
    B: Backend,
{
    let mut text = [Text::raw("Battle now!")];
    match choice {
        1 => {
            text = [Text::raw("Build a team")];
        }
        _ => {}
    };
    Paragraph::new(text.iter())
        .block(Block::default().borders(Borders::ALL))
        .wrap(true)
        .render(f, area);
}
