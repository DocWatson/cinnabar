use crate::ui::app::App;

use super::super::super::battle::Battle;

use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, Gauge, Paragraph, SelectableList, Text, Widget};
use tui::Frame;

pub fn draw<B>(f: &mut Frame<B>, app: &App, area: Rect)
where
    B: Backend,
{
    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Percentage(60),
                Constraint::Percentage(25),
                Constraint::Percentage(15),
            ]
            .as_ref(),
        )
        .split(area);
    draw_battle(f, chunks[0], app.battle.as_ref().unwrap());
    draw_menu(f, app, chunks[1]);
    draw_info(f, chunks[2], app.battle.as_ref().unwrap());
}

fn draw_battle<B>(f: &mut Frame<B>, area: Rect, battle: &Battle)
where
    B: Backend,
{
    let constraints = vec![Constraint::Percentage(50), Constraint::Percentage(50)];
    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Vertical)
        .split(area);

    draw_enemy(f, chunks[0], battle);
    draw_player(f, chunks[1], battle);
}

fn draw_enemy<B>(f: &mut Frame<B>, area: Rect, battle: &Battle)
where
    B: Backend,
{
    let constraints = vec![Constraint::Percentage(60), Constraint::Percentage(40)];
    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Horizontal)
        .split(area);

    {
        let chunks = Layout::default()
            .constraints([Constraint::Percentage(60), Constraint::Percentage(40)].as_ref())
            .direction(Direction::Vertical)
            .split(chunks[1]);

        let text = [
            Text::raw(battle.trainers[1].name.as_ref().unwrap()),
            Text::raw("\n"),
            Text::raw(&battle.trainers[1].get_active_monster().nickname),
        ];

        Paragraph::new(text.iter())
            .block(Block::default().borders(Borders::ALL))
            .wrap(true)
            .render(f, chunks[0]);

        Gauge::default()
            .block(Block::default().title("HP").borders(Borders::ALL))
            .style(Style::default().fg(Color::Green))
            .percent(100)
            .label("")
            .render(f, chunks[1]);
    }
}

fn draw_player<B>(f: &mut Frame<B>, area: Rect, battle: &Battle)
where
    B: Backend,
{
    let constraints = vec![Constraint::Percentage(40), Constraint::Percentage(60)];
    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Horizontal)
        .split(area);

    {
        let chunks = Layout::default()
            .constraints([Constraint::Percentage(60), Constraint::Percentage(40)].as_ref())
            .direction(Direction::Vertical)
            .split(chunks[0]);

        let active_mon = battle.trainers[0].get_active_monster();

        let text = [
            Text::raw(battle.trainers[0].name.as_ref().unwrap()),
            Text::raw("\n"),
            Text::raw(&active_mon.nickname),
        ];

        Paragraph::new(text.iter())
            .block(Block::default().borders(Borders::ALL))
            .wrap(true)
            .render(f, chunks[0]);

        Gauge::default()
            .block(Block::default().title("HP").borders(Borders::ALL))
            .style(Style::default().fg(Color::Green))
            .percent(100)
            .label("")
            .render(f, chunks[1]);
    }
}

fn draw_menu<B>(f: &mut Frame<B>, app: &App, area: Rect)
where
    B: Backend,
{
    let constraints = vec![Constraint::Percentage(30), Constraint::Percentage(70)];
    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Horizontal)
        .split(area);
    {
        SelectableList::default()
            .block(Block::default().borders(Borders::ALL).title("Menu"))
            .items(&app.menu_choices.items)
            .select(Some(app.menu_choices.selected))
            .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
            .highlight_symbol(">")
            .render(f, chunks[0]);
    }
}

fn draw_info<B>(f: &mut Frame<B>, area: Rect, battle: &Battle)
where
    B: Backend,
{
    // TODO read from the latest battle.events and print it out
    let text = [Text::raw("Two Trainers want to fight!")];

    Paragraph::new(text.iter())
        .block(Block::default().borders(Borders::ALL))
        .wrap(true)
        .render(f, area);
}
