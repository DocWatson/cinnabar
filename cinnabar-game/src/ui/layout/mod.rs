mod battle;
mod team;
mod title;

use std::io;

use tui::backend::Backend;
use tui::layout::{Constraint, Layout};
use tui::Terminal;

use crate::ui::app::App;

pub fn draw<B: Backend>(terminal: &mut Terminal<B>, app: &mut App) -> Result<(), io::Error> {
    terminal.draw(|mut f| {
        let chunks = Layout::default()
            .constraints([Constraint::Length(1), Constraint::Min(0)].as_ref())
            .split(f.size());

        // this probably needs to be smarter...
        match app.mode {
            Some(0) => {
                battle::draw(&mut f, app, chunks[1]);
            }
            Some(1) => {
                team::draw(&mut f, app, chunks[1]);
            }

            _ => title::draw(&mut f, app, chunks[1]),
        }
    })
}
