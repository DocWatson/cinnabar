use crate::ui::app::App;

use super::super::super::team;

use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, Paragraph, SelectableList, Text, Widget};
use tui::Frame;

pub fn draw<B>(f: &mut Frame<B>, app: &App, area: Rect)
where
    B: Backend,
{
    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Percentage(60),
                Constraint::Percentage(25),
                Constraint::Percentage(15),
            ]
            .as_ref(),
        )
        .split(area);
    draw_info(f, chunks[0]);
    draw_menu(f, app, chunks[1]);
}

fn draw_info<B>(f: &mut Frame<B>, area: Rect)
where
    B: Backend,
{
    // TODO read from the latest battle.events and print it out
    let text = [Text::raw("Make a Team")];

    Paragraph::new(text.iter())
        .block(Block::default().borders(Borders::ALL))
        .wrap(true)
        .render(f, area);
}

fn draw_menu<B>(f: &mut Frame<B>, app: &App, area: Rect)
where
    B: Backend,
{
    let constraints = vec![Constraint::Percentage(30), Constraint::Percentage(70)];
    let chunks = Layout::default()
        .constraints(constraints)
        .direction(Direction::Horizontal)
        .split(area);
    {
        SelectableList::default()
            .block(Block::default().borders(Borders::ALL).title("Menu"))
            .items(&app.menu_choices.items)
            .select(Some(app.menu_choices.selected))
            .highlight_style(Style::default().fg(Color::Yellow).modifier(Modifier::BOLD))
            .highlight_symbol(">")
            .render(f, chunks[0]);
    }
}
