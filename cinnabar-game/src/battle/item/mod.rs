use crate::battle::Battle;
use crate::traits::Performable;

#[derive(Debug, Clone)]
pub struct Item {
    name: String,
}

pub struct ItemAction {
    item: Item,
    priority_score: i32,
}

impl Performable for ItemAction {
    fn perform(&self, _battle: &mut Battle) {
        println!("Using {}", &self.item.name);
    }

    fn get_priority_score(&self) -> i32 {
        self.priority_score
    }
}
