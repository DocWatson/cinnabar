use std::io::{stdin, stdout, Write};

extern crate colored;
use self::colored::*;

extern crate cinnabar_types;
use self::cinnabar_types::{Pokemon, PokemonMove};

use crate::battle::trainer::PokemonTrainer;
use crate::battle::Battle;

use crate::battle::battle_action::BattleAction;

pub fn get_input() -> String {
    let mut s = String::new();
    let _ = stdout().flush();

    stdin()
        .read_line(&mut s)
        .expect("Did not enter a correct string");

    let len_withoutcrlf = s.len();
    s.truncate(len_withoutcrlf);

    s
}

fn get_first_char(s: String) -> char {
    s.chars().next().or(Some('X')).unwrap()
}

fn list_moves(available_moves: &Vec<PokemonMove>) {
    for (ctr, a_move) in available_moves.iter().enumerate() {
        println!("{}.) {:?}", ctr + 1, a_move.identifier);
    }
}

fn list_pokemon(pokemon_team: &Vec<Pokemon>) {
    for (ctr, a_mon) in pokemon_team.iter().enumerate() {
        println!(
            "{}.) {:?} ({} / {})",
            ctr + 1,
            a_mon.nickname,
            a_mon.hp.current(),
            a_mon.hp.maximum()
        );
    }
}

pub fn battle_menu(battle: &Battle, trainer_index: usize) -> BattleAction {
    let trainer = battle.get_trainer(trainer_index);

    let move_list = trainer.list_battle_moves();

    println!("\n{}\n", "What will you do?".bold());
    println!("{}", "MOVES".cyan());
    self::list_moves(move_list);
    println!("{}) Pokémon", "P".red());
    println!("{}) Items", "I".green());

    print!("Selection: ");
    let menu_choice = self::get_first_char(self::get_input());

    let action = self::get_battle_menu_selection(menu_choice, trainer);

    if action == BattleAction::InvalidChoice {
        println!("\n!! {} !!\n", "INVALID SELECTION".red().bold());
        self::battle_menu(battle, trainer_index)
    } else if action == BattleAction::Cancel {
        self::battle_menu(battle, trainer_index)
    } else {
        return action;
    }
}

fn get_battle_menu_selection(menu_choice: char, trainer: &PokemonTrainer) -> BattleAction {
    match menu_choice {
        '1'..='4' => BattleAction::PerformMove(self::char_to_usize(menu_choice) - 1),
        'P' | 'p' => self::choose_new_monster(trainer),
        'I' | 'i' => self::choose_item(trainer),
        _ => BattleAction::InvalidChoice,
    }
}

/// Called when a swap *must* happen, such as when a monster faints or is scared away
pub fn force_swap_menu(battle: &mut Battle, trainer_index: usize) {
    let battle_copy = battle.clone();
    let trainer = battle_copy.get_trainer(trainer_index);
    println!("{}", "Who will you send out?".bold());
    self::list_pokemon(trainer.get_team());
    print!("{}", "Choose a monster: ".bold());
    let menu_choice = self::char_to_usize(self::get_first_char(self::get_input()));

    if trainer.get_valid_swap_choice(menu_choice) {
        let new_trainer = PokemonTrainer::send_out_monster(trainer.clone(), menu_choice - 1);
        battle.update_trainer(trainer_index, new_trainer);
    } else {
        println!("{}", "!! Invalid !!".red());
        self::force_swap_menu(battle, trainer_index)
    }
}

fn choose_new_monster(trainer: &PokemonTrainer) -> BattleAction {
    println!("{}", "Your Team:".bold());
    self::list_pokemon(trainer.get_team());
    println!("{}) Cancel", "X".cyan());
    print!("{}", "Choose a monster: ".bold());
    let menu_choice = self::get_first_char(self::get_input());
    self::get_team_choice(menu_choice, trainer)
}

fn get_team_choice(menu_choice: char, trainer: &PokemonTrainer) -> BattleAction {
    match menu_choice {
        '1'..='6' => self::get_monster_selection(menu_choice, trainer),
        'X' | 'x' => BattleAction::Cancel,
        _ => BattleAction::InvalidChoice,
    }
}

fn get_monster_selection(menu_choice: char, trainer: &PokemonTrainer) -> BattleAction {
    let choice: usize = self::char_to_usize(menu_choice);

    if !trainer.get_valid_swap_choice(choice) {
        println!("{}", "That Pokémon is already out!".red());
        BattleAction::Cancel
    } else {
        BattleAction::Swap(choice)
    }
}

fn choose_item(_trainer: &PokemonTrainer) -> BattleAction {
    BattleAction::Item
}

fn char_to_usize(c: char) -> usize {
    c.to_digit(10).unwrap() as usize
}
