extern crate cinnabar_types;
use self::cinnabar_types::{Pokemon, PokemonMove};

use crate::battle::item::Item;

#[derive(Debug, Clone)]
pub struct PokemonTrainer {
    pub name: Option<String>,
    team: Vec<Pokemon>,
    active_monster: Option<usize>,
    is_playable: bool,
    inventory: Vec<Item>,
}

impl PokemonTrainer {
    pub fn new(name: String, playable: bool) -> PokemonTrainer {
        PokemonTrainer {
            name: Some(name),
            team: vec![],
            active_monster: None,
            is_playable: playable,
            inventory: vec![],
        }
    }

    pub fn add_to_team(&mut self, monster: Pokemon) {
        &self.team.push(monster);
    }

    pub fn send_out_monster(current_trainer: PokemonTrainer, position: usize) -> PokemonTrainer {
        {
            let monster = &current_trainer.team[position];
        }
        PokemonTrainer {
            name: current_trainer.name,
            team: current_trainer.team,
            active_monster: Some(position),
            is_playable: current_trainer.is_playable,
            inventory: current_trainer.inventory,
        }
    }

    pub fn get_active_monster(&self) -> &Pokemon {
        &self.team[self.get_active_monster_position()]
    }

    pub fn get_active_monster_position(&self) -> usize {
        self.active_monster.unwrap()
    }

    pub fn get_active_monster_move(&self, move_index: usize) -> &PokemonMove {
        let active_monster = self.get_active_monster();
        &active_monster.moves[move_index]
    }

    pub fn get_team(&self) -> &Vec<Pokemon> {
        &self.team
    }

    pub fn get_target(&self, trainer_index: usize) -> usize {
        match trainer_index {
            0 => 1,
            1 => 0,
            _ => unreachable!(),
        }
    }

    pub fn list_battle_moves(&self) -> &Vec<PokemonMove> {
        let active_monster = &self.get_active_monster();
        &active_monster.moves
    }

    pub fn list_inventory(&self) -> &Vec<Item> {
        &self.inventory
    }

    pub fn select_move(&self, choice: usize) -> &PokemonMove {
        let active_monster_moves = &self.list_battle_moves();

        &active_monster_moves[choice]
    }

    pub fn is_playable(&self) -> bool {
        self.is_playable
    }

    pub fn get_valid_swap_choice(&self, index: usize) -> bool {
        let position = index - 1;

        if index > self.team.len() {
            println!("OUT OF BOUNDS");
            return false;
        } else if self.team[position].is_fainted() {
            println!("FAINTED CHOICE");
            return false;
        } else if position == self.get_active_monster_position() {
            println!("ACTIVE CHOICE");
            return false;
        }

        return true;
    }

    pub fn update_monster_at_position(&mut self, pokemon: Pokemon, position: usize) {
        self.team[position] = pokemon
    }

    pub fn update_active_monster(&mut self, pokemon: Pokemon) {
        let active_pos = self.get_active_monster_position();
        self.update_monster_at_position(pokemon, active_pos);
    }
}
