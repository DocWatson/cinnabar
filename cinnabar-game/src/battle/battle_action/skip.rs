use crate::traits::Performable;
use crate::battle::Battle;

/// Struct to support skip behaviors
pub struct Skip {}

impl Performable for Skip {
    fn perform(&self, _battle: &mut Battle) {
        println!("Skipping...");
    }

    fn get_priority_score(&self) -> i32 {
        -15
    }
}