pub mod skip;
pub mod swap;

#[derive(PartialEq, Clone)]
pub enum BattleAction {
    /// Pokemon will perform a move in `monster.moves[usize]`
    PerformMove(usize),
    Swap(usize),
    /// Trainer will use an item in `trainer.inventory[usize]`
    Item,
    /// Trainer cancels when given a menu choice
    Cancel,
    /// Trainer will skip a given turn
    Skip,
    /// Trainer made an invalid selection.
    /// Typically results in asking for another selection.
    InvalidChoice,
    /// Trainer or monster runs away from battle
    Flee,
    /// Futher explanations of events or actions
    Announcement,
}
