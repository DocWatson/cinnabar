use crate::traits::Performable;
use crate::battle::Battle;
use crate::battle::trainer::PokemonTrainer;

pub struct SwapAction { 
    /// The trainer performing the swap
    actor: usize,
    /// The monster to swap to
    swap_target: usize 
}

impl SwapAction {
    pub fn new(actor: usize, target: usize) -> SwapAction {
        SwapAction {
            actor: actor,
            swap_target: target - 1
        }
    }
}

impl Performable for SwapAction {
    fn perform(&self, battle: &mut Battle) {
        let current_trainer = battle.get_trainer(self.actor).clone();
        let updated_trainer = PokemonTrainer::send_out_monster(current_trainer, self.swap_target);
        battle.update_trainer(self.actor, updated_trainer);
    }

    fn get_priority_score(&self) -> i32 {
        0
    }
}