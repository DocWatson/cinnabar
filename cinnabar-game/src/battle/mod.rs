use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;

use std::cmp::Ordering;

extern crate rand;
use self::rand::{thread_rng, Rng};

extern crate cinnabar_types;
use self::cinnabar_types::{Pokemon, PokemonMove, PokemonSpecies};

pub mod battle_action;
use self::battle_action::skip::Skip;
use self::battle_action::swap::SwapAction;
use self::battle_action::BattleAction;
mod tests;

pub mod item;
pub mod menu;
pub mod trainer;

use crate::pokemon::moves::PokemonMoveAction;

use self::trainer::PokemonTrainer;

use super::team;

use crate::traits::Performable;

#[derive(Clone)]
pub struct Battle {
    pub trainers: Vec<PokemonTrainer>,
    turn_number: u32,
    escapable: bool,
    events: Vec<BattleAction>,
}

impl Battle {
    pub fn create_battle() -> Battle {
        // TODO: don't read these files in here....
        // should load in chosen team files
        // pass in? read from app.teams?
        let pokemon_file = File::open("../cinnabar-data/output/pokemon.json").unwrap();
        let pf_reader = BufReader::new(pokemon_file);
        // Read the JSON contents of the file as an instance of `User`.
        let pokemon_data: Vec<PokemonSpecies> = serde_json::from_reader(pf_reader).unwrap();
        let moves_file = File::open("../cinnabar-data/output/moves.json").unwrap();
        let mv_reader = BufReader::new(moves_file);
        // Read the JSON contents of the file as an instance of `User`.
        let move_data: HashMap<i32, PokemonMove> = serde_json::from_reader(mv_reader).unwrap();

        let mut player1 = PokemonTrainer::new("YOU".to_string(), true);
        let mut player2 = PokemonTrainer::new("Youngster Joey".to_string(), false);

        let mut pikachu = Pokemon::new("Pikachu", 10, &pokemon_data[24]);
        let mut mankey = Pokemon::new("Mankey", 5, &pokemon_data[55]);
        let mut ratata = Pokemon::new("Ratata", 11, &pokemon_data[18]);

        let quick_attack = move_data.get(&98).unwrap().clone();
        let karate_chop = move_data.get(&2).unwrap().clone();

        pikachu.learn_move(quick_attack);
        // pikachu.learn_move(thunder_shock);

        mankey.learn_move(karate_chop);

        let quick_attack = move_data.get(&98).unwrap().clone();
        // ratata.learn_move(tail_whip);
        ratata.learn_move(quick_attack);

        player1.add_to_team(pikachu);
        player2.add_to_team(ratata);
        player1.add_to_team(mankey);

        Battle::new(player1, player2)
    }

    pub fn new(player1: PokemonTrainer, player2: PokemonTrainer) -> Battle {
        let trainer1 = PokemonTrainer::send_out_monster(player1, 0);
        let trainer2 = PokemonTrainer::send_out_monster(player2, 0);

        let mut trainers = vec![];
        trainers.push(trainer1);
        trainers.push(trainer2);

        Battle {
            trainers: trainers,
            turn_number: 0,
            escapable: false,
            events: vec![],
        }
    }

    pub fn start_battle(&mut self) {
        self.battle_turn();
    }

    fn battle_turn(&mut self) {
        if !self.is_battle_over() {
            let trainer_count = &self.trainers.len();
            self.turn_number += 1;
        // let mut player_choices = Vec::with_capacity(*trainer_count);
        // for trainer in 0..*trainer_count {
        //     let choice = self.get_player_turn_choice(trainer);
        //     player_choices.push(choice);
        // }

        // self.perform_actions(&player_choices);

        // self.battle_turn();
        } else {
            println!("GAME OVER MoN");
        }
    }

    pub fn get_trainer(&self, trainer_index: usize) -> &PokemonTrainer {
        &self.trainers[trainer_index]
    }

    pub fn get_trainer_active_monster(&self, trainer_index: usize) -> &Pokemon {
        let trainer = &self.get_trainer(trainer_index);
        &trainer.get_active_monster()
    }

    pub fn update_trainer(&mut self, trainer_index: usize, trainer: PokemonTrainer) {
        self.trainers[trainer_index] = trainer;
    }

    pub fn update_trainer_monster(&mut self, trainer_index: usize, monster: Pokemon) {
        self.trainers[trainer_index].update_active_monster(monster);
    }

    fn get_player_turn_choice(&self, trainer_index: usize) -> BattleAction {
        let trainer = self.get_trainer(trainer_index);

        if trainer.is_playable() {
            let opp_move = trainer.get_active_monster().get_random_move();
            BattleAction::PerformMove(opp_move)
        // menu::battle_menu(self, trainer_index)
        } else {
            let opp_move = trainer.get_active_monster().get_random_move();
            BattleAction::PerformMove(opp_move)
        }
    }

    fn perform_actions(&mut self, player_choices: &Vec<BattleAction>) {
        let mut execution_order: Vec<Box<Performable>> = vec![];

        for (idx, choice) in player_choices.iter().enumerate() {
            let performable = self.get_performable(idx, choice);
            execution_order.push(performable);
        }

        execution_order.sort_by(|a, b| {
            b.get_priority_score()
                .partial_cmp(&a.get_priority_score())
                .unwrap_or(Ordering::Equal)
        });

        for action in execution_order {
            action.perform(self);
        }
    }

    fn get_performable(&self, trainer_index: usize, action: &BattleAction) -> Box<Performable> {
        match action {
            &BattleAction::PerformMove(move_index) => {
                let actor = trainer_index;
                let actor_trainer = &self.get_trainer(actor);
                let move_to_do = actor_trainer.get_active_monster_move(move_index).clone();
                let target = actor_trainer.get_target(trainer_index);

                let move_action = PokemonMoveAction::new(move_to_do, actor, target, &self);
                Box::new(move_action)
            }
            &BattleAction::Swap(swap_to_monster) => {
                let actor = trainer_index;
                let swap_action = SwapAction::new(actor, swap_to_monster);
                Box::new(swap_action)
            }
            // BattleAction::Item => println!("ITEM!"),
            _ => Box::new(Skip {}),
        }
    }

    fn compare_pokemon_speed(&self, p1_mon: &Pokemon, p2_mon: &Pokemon) -> u8 {
        if p1_mon.battle_statistics.speed == p2_mon.battle_statistics.speed {
            rand::thread_rng().gen_range(1, 2)
        } else if p1_mon.battle_statistics.speed > p2_mon.battle_statistics.speed {
            return 1;
        } else {
            return 2;
        }
    }

    pub fn is_battle_over(&self) -> bool {
        for trainer in &self.trainers {
            let mut all_monsters_dead = true;
            for monster in trainer.get_team().iter() {
                if !monster.is_fainted() {
                    all_monsters_dead = false;
                }
            }

            if all_monsters_dead {
                return true;
            }
        }

        return false;
    }
}
