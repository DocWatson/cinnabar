use crate::battle::Battle;

pub trait Performable {
    fn perform(&self, battle: &mut Battle);
    fn get_priority_score(&self) -> i32;
}

// pub trait Battle {
//     fn start(&self);
//     fn is_battle_over(&self);
// }