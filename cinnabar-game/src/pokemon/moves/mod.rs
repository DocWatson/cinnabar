extern crate cinnabar_types;
use self::cinnabar_types::{PokemonMove, PokemonType, Stat};

use crate::traits::Performable;

use super::battle;

pub struct PokemonMoveAction {
    action: PokemonMove,
    actor: usize,
    target: usize,
    priority_score: i32,
}

impl PokemonMoveAction {
    pub fn new(
        action: PokemonMove,
        actor: usize,
        target: usize,
        battle: &battle::Battle,
    ) -> PokemonMoveAction {
        let mut priority_score = action.priority.unwrap_or(0);
        let attacker = battle.get_trainer_active_monster(actor);
        let defender = battle.get_trainer_active_monster(target);

        if attacker.battle_statistics.speed > defender.battle_statistics.speed {
            priority_score += 5;
        }

        PokemonMoveAction {
            action: action,
            actor: actor,
            target: target,
            priority_score: priority_score,
        }
    }

    fn calculate_damage(&self, battle: &battle::Battle) -> i32 {
        let mut damage = 0;

        if self.action.power.unwrap() > 0 {
            let attacker = battle.get_trainer_active_monster(self.actor);
            let defender = battle.get_trainer_active_monster(self.target);

            let level = attacker.level;
            let attacker_stat = attacker.battle_statistics.get_stat_value(&Stat::Attack);
            let defender_stat = defender.battle_statistics.get_stat_value(&Stat::Defense);

            damage = (((((2 * level) / 5) + 2)
                * self.action.power.unwrap()
                * (attacker_stat / defender_stat))
                / 50)
                + 2;
        }
        damage
    }

    /// Check the status of the actor to make sure it can proceed with attacking
    fn actor_can_act(&self, battle: &mut battle::Battle) -> bool {
        let acting_monster = battle.get_trainer_active_monster(self.actor);

        !acting_monster.is_fainted()
    }
}

impl Performable for PokemonMoveAction {
    fn perform(&self, battle: &mut battle::Battle) {
        if self.actor_can_act(battle) {
            let damage = self.calculate_damage(battle);
            println!("Doing move {} for {} dmg", &self.action.identifier, damage);
            let mut target_mon = battle.get_trainer_active_monster(self.target).clone();
            target_mon.take_damage(damage);

            if target_mon.hp.current() < 1 {
                // TODO: fainted announcement
                battle.update_trainer_monster(self.target, target_mon);
                if !battle.is_battle_over() {}
            } else {
                battle.update_trainer_monster(self.target, target_mon);
            }
        }
    }

    fn get_priority_score(&self) -> i32 {
        self.priority_score
    }
}
