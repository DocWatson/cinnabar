use pokemon::stats::Stat;

#[derive(Debug, Clone, PartialEq)]
pub enum EffectType {
    StatModification { affected_stat: Stat, change: f32, },
    Poison,
    Burn,
    Paralyze,
    Freeze,
    Sleep,
    Confusion,
    Faint
}

#[derive(Debug, Clone)]
pub struct SecondaryEffect {
    pub effect_type: EffectType,
    pub chance: f32
}