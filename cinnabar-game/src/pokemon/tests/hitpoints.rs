#[cfg(test)]
mod pokemon_hp_test {
    use pokemon::Pokemon;

    #[test]
    fn take_and_heal_damage() {
        let mut pokemon = Pokemon::new("Pikachu", 10);
        pokemon.take_damage(11);
        assert_eq!(pokemon.hp.current(), pokemon.hp.maximum() - 11);

        pokemon.heal(10);
        assert_eq!(pokemon.hp.current(), pokemon.hp.maximum() - 1);

        pokemon.heal(10);
        assert!(pokemon.is_full_health());
    }

    #[test]
    fn insta_faint() {
        let mut pokemon = Pokemon::new("Pikachu", 10);

        pokemon.faint();
        assert!(pokemon.is_fainted());
    }

    #[test]
    fn recover() {
        let mut pokemon = Pokemon::new("Pikachu", 10);

        pokemon.hp -= 30;
        pokemon.full_recover();
        assert!(pokemon.is_full_health());
    }
}