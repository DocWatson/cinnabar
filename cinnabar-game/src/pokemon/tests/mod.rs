mod hitpoints;
mod status;

#[cfg(test)]
mod pokemon_test {
    use pokemon::Pokemon;
    use pokemon::nature;
    use pokemon::stats;

    #[test]
    fn create_monster() {
        let pokemon = Pokemon::new("Pikachu", 10);
        assert_eq!(pokemon.nickname, "Pikachu");
    }

    #[test]
    fn create_neutral_nature() {
        let test_nature = nature::Natures::Hardy.value();
        assert_eq!(test_nature.is_neutral(), true);
    }

    #[test]
    fn create_non_neutral_nature() {
        let test_nature = nature::Natures::Lonely.value();
        assert_eq!(test_nature.is_neutral(), false);
    }

    #[test]
    fn create_and_add_stats() {
        let monster_stats_one = stats::Statistics::new(1, 1, 1, 1, 1, 1);
        let monster_stats_two = stats::IndividualValues::new(2, 2, 2, 2, 2, 2);
        let added_stats = monster_stats_one.add(monster_stats_two);

        let expected_stats = stats::Statistics::new(3, 3, 3, 3, 3, 3);

        assert_eq!(expected_stats.equals(added_stats), true);
    }
}
