#[cfg(test)]
mod pokemon_hp_test {
    use pokemon::Pokemon;
    use pokemon::moves::secondary_effects::EffectType;

    #[test]
    fn paralyze() {
        let mut pokemon = Pokemon::new("Pikachu", 10);
        pokemon.set_status(Some(EffectType::Paralyze));

        assert_eq!(pokemon.status().unwrap(), EffectType::Paralyze);
    }

    #[test]
    fn paralyz_heal() {
        let mut pokemon = Pokemon::new("Pikachu", 10);
        pokemon.set_status(Some(EffectType::Paralyze));
        pokemon.set_status(None);
        assert!(pokemon.status().is_none());
    }
}