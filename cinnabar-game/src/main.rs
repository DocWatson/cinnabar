use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;

extern crate cinnabar_types;
use self::cinnabar_types::{PokemonMove, PokemonSpecies};

use std::io;
use std::time::Duration;

use termion::event::Key;
use termion::input::MouseTerminal;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::Terminal;

mod ui;
use crate::ui::util::{Config, Event, Events};
use crate::ui::{layout, App};

pub mod battle;
mod pokemon;
mod team;
mod traits;

fn main() -> Result<(), failure::Error> {
    // Open the file in read-only mode with buffer.
    let pokemon_file = File::open("../cinnabar-data/output/pokemon.json").unwrap();
    let pf_reader = BufReader::new(pokemon_file);

    // Read the JSON contents of the file as an instance of `User`.
    let pokemon_data: Vec<PokemonSpecies> = serde_json::from_reader(pf_reader).unwrap();

    let moves_file = File::open("../cinnabar-data/output/moves.json").unwrap();
    let mv_reader = BufReader::new(moves_file);

    // Read the JSON contents of the file as an instance of `User`.
    let move_data: HashMap<i32, PokemonMove> = serde_json::from_reader(mv_reader).unwrap();

    let events = Events::with_config(Config {
        tick_rate: Duration::from_millis(250),
        ..Config::default()
    });

    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    let mut app = App::new("Cinnabar", pokemon_data, move_data);
    loop {
        layout::draw(&mut terminal, &mut app)?;
        match events.next()? {
            Event::Input(key) => match key {
                Key::Char(c) => {
                    app.on_key(c);
                }
                Key::Up => {
                    app.on_up();
                }
                Key::Down => {
                    app.on_down();
                }
                _ => {}
            },
            Event::Tick => {
                app.on_tick();
            }
        }
        if app.should_quit {
            break;
        }
    }

    Ok(())
}
