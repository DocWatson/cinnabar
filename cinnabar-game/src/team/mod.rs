use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

extern crate cinnabar_types;
use self::cinnabar_types::{Pokemon, PokemonMove, PokemonSpecies};

pub enum DefaultIV {
    Random,
    Perfect,
    None,
}

/// Represents the settings for building teams
/// TODO: allow these to be configurable
pub struct TeamBuilderSettings {
    pub default_level: i32,
    pub default_iv: DefaultIV,
    pub max_team_size: i32,
}

impl Default for TeamBuilderSettings {
    fn default() -> TeamBuilderSettings {
        TeamBuilderSettings {
            default_level: 50,
            default_iv: DefaultIV::Random,
            max_team_size: 6,
        }
    }
}

/// Kicks off the Create-A-Team wizard
pub fn create_team() {
    // ask if the user wants to
    // 1. Manually Create a Team
    // 2. Randomly generate a team
    // 3. Load a Team from a file
    // 4. Cancel
}

/// Create a completely random team of a given size
/// if less than 1, a random team size will be chosen
/// if greater than 6, clamps to 6
/// TODO: create a TeamConstraints type to make refined rules for team generation
pub fn create_random_team(size: i32) {}

pub fn create_test_team() {
    // TODO: don't read these files in here....
    // should load in chosen team files
    // pass in? read from app.teams?
    let pokemon_file = File::open("../cinnabar-data/output/pokemon.json").unwrap();
    let pf_reader = BufReader::new(pokemon_file);
    // Read the JSON contents of the file as an instance of `User`.
    let pokemon_data: Vec<PokemonSpecies> = serde_json::from_reader(pf_reader).unwrap();
    let moves_file = File::open("../cinnabar-data/output/moves.json").unwrap();
    let mv_reader = BufReader::new(moves_file);
    // Read the JSON contents of the file as an instance of `User`.
    let move_data: HashMap<i32, PokemonMove> = serde_json::from_reader(mv_reader).unwrap();

    let mut pokemon = vec![];

    let mut pikachu = Pokemon::new("Pikachu", 10, &pokemon_data[24]);
    let mut mankey = Pokemon::new("Mankey", 5, &pokemon_data[55]);
    let mut ratata = Pokemon::new("Ratata", 11, &pokemon_data[18]);

    let quick_attack = move_data.get(&98).unwrap().clone();
    let karate_chop = move_data.get(&2).unwrap().clone();

    pikachu.learn_move(quick_attack);
    // pikachu.learn_move(thunder_shock);

    mankey.learn_move(karate_chop);

    let quick_attack = move_data.get(&98).unwrap().clone();
    // ratata.learn_move(tail_whip);
    ratata.learn_move(quick_attack);

    pokemon.push(pikachu);
    pokemon.push(mankey);

    let result = write_team_file(&pokemon, "test".to_string());
}

pub fn list_monsters() {
    // list all the monsters by id
    // bonus tab options: order by id | order by name | order by type | region
    // choosing a monster calls -> choose monster
    // if team size < 6, allow choose again
    // otherwise allow player to complete team build
}

/// Given the vec of PokemonSpecies (reminder, these are ordered sequentially by ID)
/// and an ID, create a monster
pub fn choose_monster(pokemon_data: Vec<PokemonSpecies>, id: usize) {
    let pokemon_species = &pokemon_data[id];
    let monster = Pokemon::new(&pokemon_species.identifier, 50, pokemon_species);
    // after selecting a monster
    // ask for name (default: monster species name)
    // ask for nature (default: random nature, but allow player to regenerate or select manually)
    // ask for level (default 50)
    // set moves (get 4 random moves, choose from list of learnable moves, default most recent 4 based on level)
    // display monster name, stats, nature, and moves and ask: ok | make change | cancel
    // if okay, return monster and add to team list
    // make changes, begin the steps above with the previous selections
    // if cancel, prompt player to verify and exit (can be done any time pressing esc)
}

pub fn choose_move_for_monster(available_moves: Vec<PokemonMove>) {
    // list all the moves a monster can learn
    // tab options: order by level | order by name | order by type | order by learn method
}

fn get_team_name() -> String {
    // TODO: ask for player input
    return "My Team".to_string();
}

pub fn build_team() -> Result<(), Box<dyn Error>> {
    let monster_list: Vec<Pokemon> = vec![];
    // get Vec of monsters (max: 6, min: 1)

    let team_name = get_team_name();

    write_team_file(&monster_list, team_name)
}

pub fn write_team_file(team_data: &Vec<Pokemon>, team_name: String) -> Result<(), Box<dyn Error>> {
    // create a path in the teams directory with the given name
    let path = Path::new("teams").join(team_name).with_extension("json");
    // todo: don't write pretty to save space?
    let _result = serde_json::to_writer_pretty(&File::create(path)?, team_data);
    Ok(())
}
