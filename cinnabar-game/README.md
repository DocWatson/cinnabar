# Cinnabar

A Pokemon Battle Simulator built in Rust

### Development

- Install rust (recommend using [rustup](https://www.rustup.rs/))
   - Built using rust 1.15.1 (older versions may not work)
- Run with `cargo run` -- will install the required deps for you
- Test with `cargo test`
- Build with `cargo build`
