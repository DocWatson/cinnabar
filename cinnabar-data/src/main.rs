extern crate csv;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

extern crate cinnabar_types;

use std::fs::File;

use std::collections::HashMap;
use std::error::Error;

use cinnabar_types::{PokemonMove, PokemonSpecies, PokemonStats, PokemonType};

const Language: i32 = 9;

#[derive(Debug, Deserialize)]
struct PokemonCSV {
    id: i32,
    identifier: String,
}

#[derive(Debug, Deserialize)]
struct LanguageCSV {
    id: i32,
    iso639: String,
    identifier: String,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
struct MoveCSV {
    id: i32,
    identifier: String,
    generation_id: i32,
    type_id: i32,
    power: Option<i32>,
    pp: Option<i32>,
    accuracy: Option<i32>,
    priority: Option<i32>,
    target_id: Option<i32>,
    damage_class_id: i32,
    effect_id: i32,
    effect_chance: Option<i32>,
}

#[derive(Debug, Deserialize)]
struct TypeCSV {
    id: i32,
    identifier: String,
    generation_id: i32,
    damage_class_id: Option<i32>,
}

#[derive(Debug, Deserialize)]
struct TypeNameCSV {
    type_id: i32,
    local_language_id: i32,
    name: String,
}

#[derive(Debug, Deserialize)]
struct PokemonTypeCSV {
    pokemon_id: i32,
    type_id: i32,
    slot: usize,
}

#[derive(Debug, Deserialize)]
struct StatCSV {
    id: i32,
    damage_class_id: Option<i32>,
    identifier: String,
}

#[derive(Debug, Deserialize)]
struct StatNameCSV {
    stat_id: i32,
    local_language_id: i32,
    name: String,
}

#[derive(Debug, Deserialize)]
struct PokemonStatCSV {
    pokemon_id: i32,
    stat_id: i32,
    base_stat: i32,
    effort: i32,
}

#[derive(Debug, Deserialize)]
struct PokemonMoveCSV {
    pokemon_id: i32,
    version_group_id: i32,
    move_id: i32,
    pokemon_move_method_id: i32,
    level: i32,
}

fn read_languages() -> Result<HashMap<i32, LanguageCSV>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/languages.csv")?;
    let mut lang_map = HashMap::new();

    for result in rdr.deserialize() {
        let record: LanguageCSV = result?;
        lang_map.insert(record.id, record);
    }
    Ok(lang_map)
}

fn read_pokemon() -> Result<HashMap<i32, PokemonCSV>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/pokemon.csv")?;
    let mut pokemon_map = HashMap::new();

    for result in rdr.deserialize() {
        let record: PokemonCSV = result?;
        pokemon_map.insert(record.id, record);
    }

    Ok(pokemon_map)
}

fn read_types() -> Result<HashMap<i32, TypeCSV>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/types.csv")?;
    let mut type_map = HashMap::new();

    for result in rdr.deserialize() {
        let record: TypeCSV = result?;
        type_map.insert(record.id, record);
    }
    Ok(type_map)
}

fn read_type_names() -> Result<HashMap<i32, HashMap<i32, String>>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/type_names.csv")?;
    let mut type_name_map: HashMap<i32, HashMap<i32, String>> = HashMap::new();

    for result in rdr.deserialize() {
        let record: TypeNameCSV = result?;

        match type_name_map.contains_key(&record.local_language_id) {
            true => {
                let local_map = type_name_map.get_mut(&record.local_language_id).unwrap();
                local_map.insert(record.type_id, record.name);
            }
            false => {
                let mut sub_map = HashMap::new();
                sub_map.insert(record.type_id, record.name);
                type_name_map.insert(record.local_language_id, sub_map);
            }
        }
    }

    Ok(type_name_map)
}

fn read_pokemon_types(
    type_names: &HashMap<i32, String>,
) -> Result<HashMap<i32, Vec<PokemonType>>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/pokemon_types.csv")?;
    let mut pokemon_type_map: HashMap<i32, Vec<PokemonType>> = HashMap::new();

    for result in rdr.deserialize() {
        let record: PokemonTypeCSV = result?;

        let localized_name = type_names.get(&record.type_id).unwrap();

        let type_data = PokemonType {
            id: record.type_id,
            name: localized_name.to_string(),
        };

        match pokemon_type_map.contains_key(&record.pokemon_id) {
            true => {
                let types = pokemon_type_map.get_mut(&record.pokemon_id).unwrap();
                types.push(type_data);
            }
            false => {
                let mut types = vec![];
                types.push(type_data);
                pokemon_type_map.insert(record.pokemon_id, types);
            }
        }
    }
    Ok(pokemon_type_map)
}

fn read_stats() -> Result<HashMap<i32, StatCSV>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/stats.csv")?;
    let mut stat_map = HashMap::new();

    for result in rdr.deserialize() {
        let record: StatCSV = result?;
        stat_map.insert(record.id, record);
    }
    Ok(stat_map)
}

fn read_stat_names() -> Result<HashMap<i32, HashMap<i32, String>>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/stat_names.csv")?;
    let mut stat_map: HashMap<i32, HashMap<i32, String>> = HashMap::new();

    for result in rdr.deserialize() {
        let record: StatNameCSV = result?;

        match stat_map.contains_key(&record.local_language_id) {
            true => {
                let local_map = stat_map.get_mut(&record.local_language_id).unwrap();
                local_map.insert(record.stat_id, record.name);
            }
            false => {
                let mut sub_map = HashMap::new();
                sub_map.insert(record.stat_id, record.name);
                stat_map.insert(record.local_language_id, sub_map);
            }
        }
    }

    Ok(stat_map)
}

fn read_pokemon_stats() -> Result<HashMap<i32, PokemonStats>, Box<dyn Error>> {
    let mut rdr = csv::Reader::from_path("csv/pokemon_stats.csv")?;
    let mut pokemon_stat_map: HashMap<i32, PokemonStats> = HashMap::new();

    for result in rdr.deserialize() {
        let record: PokemonStatCSV = result?;
        match pokemon_stat_map.contains_key(&record.pokemon_id) {
            true => {
                let local_map = pokemon_stat_map.get_mut(&record.pokemon_id).unwrap();

                // to do: don't hard code these
                match record.stat_id {
                    1 => local_map.hp = record.base_stat,
                    2 => local_map.attack = record.base_stat,
                    3 => local_map.defense = record.base_stat,
                    4 => local_map.special_attack = record.base_stat,
                    5 => local_map.special_defense = record.base_stat,
                    6 => local_map.speed = record.base_stat,
                    _ => {}
                }
            }
            false => {
                let stats = PokemonStats {
                    hp: record.base_stat,
                    attack: record.base_stat,
                    defense: record.base_stat,
                    special_attack: record.base_stat,
                    special_defense: record.base_stat,
                    speed: record.base_stat,
                };
                pokemon_stat_map.insert(record.pokemon_id, stats);
            }
        }
    }

    Ok(pokemon_stat_map)
}

fn read_moves() -> Result<HashMap<i32, MoveCSV>, Box<dyn Error>> {
    // Build the CSV reader and iterate over each record.
    let mut rdr = csv::Reader::from_path("csv/moves.csv")?;
    let mut move_map = HashMap::new();

    for result in rdr.deserialize() {
        let record: MoveCSV = result?;
        move_map.insert(record.id, record);
    }
    Ok(move_map)
}

fn read_pokemon_moves(
    move_list: HashMap<i32, MoveCSV>,
) -> Result<HashMap<i32, Vec<PokemonMove>>, Box<dyn Error>> {
    // TO DO: remove dupes due to version issues
    let mut rdr = csv::Reader::from_path("csv/pokemon_moves.csv")?;
    let mut pokemon_move_map: HashMap<i32, Vec<PokemonMove>> = HashMap::new();
    let mut used_move_map: HashMap<i32, Vec<i32>> = HashMap::new();

    for result in rdr.deserialize() {
        let record: PokemonMoveCSV = result?;
        let move_record = move_list.get(&record.move_id).unwrap();

        let move_data = PokemonMove {
            id: move_record.id,
            level: record.level,
            identifier: move_record.identifier.to_string(),
            learn_method: record.pokemon_move_method_id,
        };

        match pokemon_move_map.contains_key(&record.pokemon_id) {
            true => {
                let moves = pokemon_move_map.get_mut(&record.pokemon_id).unwrap();

                match used_move_map
                    .get(&record.pokemon_id)
                    .unwrap()
                    .contains(&move_record.id)
                {
                    // if the move id is not in the map, add it
                    false => {
                        moves.push(move_data);
                        let update_map = used_move_map.get_mut(&record.pokemon_id).unwrap();
                        update_map.push(move_record.id);
                    }
                    _ => {
                        // if the move id is in the map, skip it
                    }
                }
            }
            false => {
                let mut moves = vec![];
                let mut move_tracker = vec![];
                moves.push(move_data);
                move_tracker.push(move_record.id);
                used_move_map.insert(record.pokemon_id, move_tracker);
                pokemon_move_map.insert(record.pokemon_id, moves);
            }
        }
    }

    Ok(pokemon_move_map)
}

fn build_pokemon(
    pokemon_types: HashMap<i32, Vec<PokemonType>>,
    pokemon_stats: HashMap<i32, PokemonStats>,
    pokemon_moves: HashMap<i32, Vec<PokemonMove>>,
) -> Result<Vec<PokemonSpecies>, Box<dyn Error>> {
    let mut pokemon = vec![];

    let mut rdr = csv::Reader::from_path("csv/pokemon.csv")?;

    for result in rdr.deserialize() {
        let record: PokemonCSV = result?;

        let stats = pokemon_stats.get(&record.id);
        let moves = pokemon_moves.get(&record.id);
        let types = pokemon_types.get(&record.id);

        let monster = PokemonSpecies {
            types: types.unwrap().to_vec(),
            id: record.id,
            identifier: record.identifier,
            stats: *stats.unwrap(),
            moves: moves.unwrap().to_vec(),
        };

        pokemon.push(monster)
    }

    Ok(pokemon)
}

fn main() -> Result<(), Box<dyn Error>> {
    let _languages = read_languages()?;

    // get the stats keyed by id
    let stats = read_stats()?;
    // get the stats' names keyed by language id then stat id
    let _stat_names = read_stat_names()?;

    // get the moves keyed by id
    let moves = read_moves()?;
    serde_json::to_writer_pretty(&File::create("output/moves.json")?, &moves);

    // get the types keyed by id
    let types = read_types()?;

    let type_names = read_type_names()?;

    // let pokemon_rdr = read_pokemon()?;

    // get the types, stats, and moves for each pokemon, each keyed by pokemon id
    let pokemon_types_rdr = read_pokemon_types(type_names.get(&Language).unwrap())?;
    let pokemon_stats = read_pokemon_stats()?;
    let pokemon_moves = read_pokemon_moves(moves)?;

    let pokemon_data = build_pokemon(pokemon_types_rdr, pokemon_stats, pokemon_moves);

    // todo: don't write pretty to save space?
    serde_json::to_writer_pretty(&File::create("output/pokemon.json")?, &pokemon_data?);

    Ok(())
}
