#[macro_use]
extern crate serde_derive;
extern crate serde_json;

extern crate rand;
use self::rand::{thread_rng, Rng};

pub mod ailment;
pub mod hitpoints;
pub mod nature;
pub mod stat;
pub mod types;

// pub use self::types::PokemonType;

pub use self::ailment::Ailment;
use self::hitpoints::HitPoints;
pub use self::stat::{IndividualValues, PokemonStats, Stat};

/// Represents a Move a Pokemon can learn, what level, and how to learn it
#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct LearnableMove {
    pub id: i32,
    pub level: i32,
    pub learn_method: i32,
}

/// Represents a Move
#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct PokemonMove {
    pub id: i32,
    pub identifier: String,
    pub type_id: i32,
    pub power: Option<i32>,
    pub pp: Option<i32>,
    pub accuracy: Option<i32>,
    pub priority: Option<i32>,
    pub target_id: Option<i32>,
    pub damage_class_id: i32,
    pub effect_id: i32,
    pub effect_chance: Option<i32>,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub enum DamageClass {
    Status,
    Physical,
    Special,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PokemonType {
    pub id: i32,
    pub name: String,
}

/// Represents a Pokemon Species
///
/// Contains id, type information, base stats, and all moves learned.
/// Not to be confused with the `Pokemon` type, which represents an individual monster.
#[derive(Debug, Deserialize, Serialize)]
pub struct PokemonSpecies {
    pub id: i32,
    pub identifier: String,
    pub types: Vec<PokemonType>,
    pub moves: Vec<LearnableMove>,
    pub stats: stat::PokemonStats,
}

/// Represents an Individual Pokemon
///
/// Not to be confused with the `PokemonSpecies` type, which represents the base monster
#[derive(Debug, Clone, Serialize)]
pub struct Pokemon {
    pub nickname: String,
    pub level: i32,
    pub hp: hitpoints::HitPoints,
    pub statistics: PokemonStats,
    pub battle_statistics: PokemonStats,
    pub ivs: PokemonStats,
    pub nature: nature::Nature,
    types: Vec<PokemonType>,
    pub moves: Vec<PokemonMove>,
    status: Option<ailment::Ailment>,
}

impl Pokemon {
    pub fn new(nickname: &str, level: i32, base_species: &PokemonSpecies) -> Pokemon {
        let monster_nature = nature::random();

        let monster_stats = PokemonStats::default();
        let ivs = IndividualValues::random();

        let initial_hp = hitpoints::HitPoints::initial(0);

        let mut monster = Pokemon {
            nickname: nickname.to_string(),
            level: level,
            types: base_species.types.to_vec(),
            hp: initial_hp,
            ivs: ivs,
            statistics: monster_stats,
            battle_statistics: monster_stats,
            nature: monster_nature,
            moves: vec![],
            status: None,
        };

        monster.recalculate_stats();

        monster
    }

    pub fn recalculate_stats(&mut self) {
        // Add IVs
        self.statistics = self.statistics.add(self.ivs);

        // TODO: add EVs

        // Scale stats by level
        self.statistics = self.statistics.multiply(self.level);
        self.statistics = self.statistics.divide(10);

        // Apply nature
        self.statistics.apply_nature(&self.nature);

        self.battle_statistics = self.statistics;

        self.hp = HitPoints::initial(self.statistics.hp);
    }

    /// Learn a new move. Take a `PokemonMove` struct and add it to the move list.
    pub fn learn_move(&mut self, new_move: PokemonMove) {
        self.moves.push(new_move);
    }

    pub fn get_random_move(&self) -> usize {
        rand::thread_rng().gen_range(0, self.moves.len())
    }

    pub fn status(self) -> Option<Ailment> {
        self.status
    }

    pub fn set_status(&mut self, status: Option<Ailment>) {
        self.status = status
    }

    pub fn take_damage(&mut self, damage: i32) {
        self.hp -= damage
    }

    /// Instantly kill the Pokemon
    pub fn faint(&mut self) {
        self.hp -= self.hp.maximum()
    }

    /// Checks to see if a Pokemon is fainted
    pub fn is_fainted(&self) -> bool {
        self.hp.current() < 1
    }

    pub fn heal(&mut self, recovery_amount: i32) {
        self.hp += recovery_amount
    }

    /// Set a Pokemon's HP to its max
    pub fn full_recover(&mut self) {
        self.hp += self.hp.maximum()
    }

    /// Checks to see if a Pokemon is at full health
    pub fn is_full_health(&self) -> bool {
        self.hp == self.hp.maximum()
    }
}
