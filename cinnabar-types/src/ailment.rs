use stat::Stat;

/// Represents an Ailment a Pokemon my suffer
#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Ailment {
    StatModification { affected_stat: Stat, change: f32 },
    Poison,
    Burn,
    Paralyze,
    Freeze,
    Sleep,
    Confusion,
    Faint,
}

/// Represents an Ailment that might be applied by a move
#[derive(Debug, Clone)]
pub struct MoveAilment {
    pub ailment: Ailment,
    pub chance: f32,
}
