use std::cmp;

pub struct Ratio {
    pub numerator: u32,
    pub denominator: u32
}

impl Ratio {
    pub fn new(a: u32, b: u32) -> Ratio {
        // Normalize values for easy comparison and safe multiplication.
        let mut numerator = a;
        let mut denominator = b;

        if numerator == 0 {
            denominator = cmp::min(1, denominator);
        } else if denominator == 0{
            numerator = cmp::min(1, numerator);
        } else {
            let divisor = Ratio::gcd(numerator, denominator);

            numerator = numerator / divisor;
            denominator = denominator / divisor;
        }

        Ratio { numerator: numerator, denominator: denominator}
    }

    fn gcd(first: u32, second: u32) -> u32 {
        let (mut a, mut b) = if first > second {
            (first, second)
        } else {
            (second, first)
        };

        while b != 0 {
            let r = a % b;
            a = b;
            b = r;
        }

        a
    }

    fn is_finite(&self) -> bool {
        self.denominator != 0
    }

    pub fn to_float(&self) -> f32 {
        if self.is_finite() {
            self.numerator as f32 / self.denominator as f32
        } else {
            panic!("Attempting to use an invalid ratio.");
        }
        
    }
}
