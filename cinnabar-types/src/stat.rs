extern crate rand;
use self::rand::{thread_rng, Rng};
use std::cmp;

use crate::nature::Nature;

#[derive(Debug, PartialEq, Clone, Serialize)]
pub enum Stat {
    Attack,
    Defense,
    Speed,
    SpecialAttack,
    SpecialDefense,
    Accuracy,
}

/// Represents the 6 main stats of a pokemon
#[derive(Debug, Deserialize, Clone, Copy, Serialize)]
pub struct PokemonStats {
    pub hp: i32,
    pub attack: i32,
    pub defense: i32,
    pub special_attack: i32,
    pub special_defense: i32,
    pub speed: i32,
}

/// Represents the 6 main stats of a pokemon as well as evasion and accuracy.
#[derive(Debug, Deserialize, Clone, Copy, Serialize)]
pub struct PokemonBattleStats {
    pub hp: i32,
    pub attack: i32,
    pub defense: i32,
    pub special_attack: i32,
    pub special_defense: i32,
    pub speed: i32,
    pub evasion: i32,
    pub accuracy: i32,
}

const INCREASE_RATIO: f32 = 11.0 / 10.0;
const DECREASE_RATIO: f32 = 9.0 / 10.0;

impl Default for PokemonStats {
    fn default() -> PokemonStats {
        PokemonStats {
            hp: 10,
            attack: 5,
            defense: 5,
            special_attack: 5,
            special_defense: 5,
            speed: 5,
        }
    }
}

impl PokemonStats {
    pub fn new(
        hp: i32,
        attack: i32,
        defense: i32,
        special_attack: i32,
        special_defense: i32,
        speed: i32,
    ) -> PokemonStats {
        PokemonStats {
            hp: hp,
            attack: attack,
            defense: defense,
            special_attack: special_attack,
            special_defense: special_defense,
            speed: speed,
        }
    }

    pub fn add(&self, other_statistics: PokemonStats) -> PokemonStats {
        PokemonStats {
            hp: self.hp + other_statistics.hp,
            attack: self.attack + other_statistics.attack,
            defense: self.defense + other_statistics.defense,
            special_attack: self.special_attack + other_statistics.special_attack,
            special_defense: self.special_defense + other_statistics.special_defense,
            speed: self.speed + other_statistics.speed,
        }
    }

    pub fn multiply(&self, scalar: i32) -> PokemonStats {
        PokemonStats {
            hp: self.hp * scalar,
            attack: self.attack * scalar,
            defense: self.defense * scalar,
            special_attack: self.special_attack * scalar,
            special_defense: self.special_defense * scalar,
            speed: self.speed * scalar,
        }
    }

    pub fn divide(&self, scalar: i32) -> PokemonStats {
        PokemonStats {
            hp: self.hp / scalar,
            attack: self.attack / scalar,
            defense: self.defense / scalar,
            special_attack: self.special_attack / scalar,
            special_defense: self.special_defense / scalar,
            speed: self.speed / scalar,
        }
    }

    pub fn apply_nature(&mut self, nature: &Nature) {
        let mut new_attack = self.attack;
        let mut new_defense = self.defense;
        let mut new_speed = self.speed;
        let mut new_spec_attack = self.special_attack;
        let mut new_spec_defense = self.special_defense;

        if !nature.is_neutral() {
            match &nature.increased_attr {
                &Stat::Attack => new_attack = (new_attack as f32 * INCREASE_RATIO) as i32,
                &Stat::Defense => new_defense = (new_defense as f32 * INCREASE_RATIO) as i32,
                &Stat::Speed => new_speed = (new_speed as f32 * INCREASE_RATIO) as i32,
                &Stat::SpecialAttack => {
                    new_spec_attack = (new_spec_attack as f32 * INCREASE_RATIO) as i32
                }
                &Stat::SpecialDefense => {
                    new_spec_defense = (new_spec_defense as f32 * INCREASE_RATIO) as i32
                }
                x => panic!("Unexpected invalid increase attribute {:?}", x),
            }

            match &nature.decreased_attr {
                &Stat::Attack => new_attack = (new_attack as f32 * DECREASE_RATIO) as i32,
                &Stat::Defense => new_defense = (new_defense as f32 * DECREASE_RATIO) as i32,
                &Stat::Speed => new_speed = (new_speed as f32 * DECREASE_RATIO) as i32,
                &Stat::SpecialAttack => {
                    new_spec_attack = (new_spec_attack as f32 * DECREASE_RATIO) as i32
                }
                &Stat::SpecialDefense => {
                    new_spec_defense = (new_spec_defense as f32 * DECREASE_RATIO) as i32
                }
                x => panic!("Unexpected invalid decrease attribute {:?}", x),
            }
        }

        self.attack = new_attack;
        self.defense = new_defense;
        self.special_attack = new_spec_attack;
        self.special_defense = new_spec_defense;
        self.speed = new_speed;
    }

    #[allow(dead_code)]
    // Check if two structs have the same values -- really only used for testing
    pub fn equals(&self, other_statistics: PokemonStats) -> bool {
        self.hp == other_statistics.hp
            && self.attack == other_statistics.attack
            && self.defense == other_statistics.defense
            && self.special_attack == other_statistics.special_attack
            && self.special_defense == other_statistics.special_defense
            && self.speed == other_statistics.speed
    }

    pub fn get_stat_value(&self, stat: &Stat) -> i32 {
        match stat {
            &Stat::Attack => self.attack,
            &Stat::SpecialAttack => self.special_attack,
            &Stat::Defense => self.defense,
            &Stat::SpecialDefense => self.special_defense,
            &Stat::Speed => self.speed,
            _ => 1,
        }
    }
}

/**********************
 *
 * INDIVIDUAL VALUES
 *
 ************************/

const MAX_IV: i32 = 31;

pub struct IndividualValues {}

impl IndividualValues {
    pub fn new(
        hp: i32,
        attack: i32,
        defense: i32,
        special_attack: i32,
        special_defense: i32,
        speed: i32,
    ) -> PokemonStats {
        PokemonStats::new(
            IndividualValues::clamp(hp),
            IndividualValues::clamp(attack),
            IndividualValues::clamp(defense),
            IndividualValues::clamp(special_attack),
            IndividualValues::clamp(special_defense),
            IndividualValues::clamp(speed),
        )
    }

    pub fn random() -> PokemonStats {
        PokemonStats::new(
            rand::thread_rng().gen_range(1, MAX_IV),
            rand::thread_rng().gen_range(1, MAX_IV),
            rand::thread_rng().gen_range(1, MAX_IV),
            rand::thread_rng().gen_range(1, MAX_IV),
            rand::thread_rng().gen_range(1, MAX_IV),
            rand::thread_rng().gen_range(1, MAX_IV),
        )
    }

    pub fn perfect() -> PokemonStats {
        PokemonStats::new(MAX_IV, MAX_IV, MAX_IV, MAX_IV, MAX_IV, MAX_IV)
    }

    fn clamp(value: i32) -> i32 {
        cmp::min(MAX_IV, value)
    }
}

const MAX_EV: i32 = 252;
const MAX_TOTAL_EV: i32 = 510;

pub struct EffortValues {}

impl EffortValues {
    pub fn new(
        max_hp: i32,
        attack: i32,
        defense: i32,
        special_attack: i32,
        special_defense: i32,
        speed: i32,
    ) -> PokemonStats {
        PokemonStats::new(
            EffortValues::clamp(max_hp, 0),
            EffortValues::clamp(attack, max_hp),
            EffortValues::clamp(defense, max_hp + attack),
            EffortValues::clamp(special_attack, max_hp + attack + defense),
            EffortValues::clamp(special_defense, max_hp + attack + defense + special_attack),
            EffortValues::clamp(
                speed,
                max_hp + attack + defense + special_attack + special_defense,
            ),
        )
    }

    // pub fn max_hp(&mut self, new_value: i32) {
    //     let total =
    //         self.attack + self.defense + self.special_attack + self.special_defense + self.speed;
    //     self.max_hp = EffortValues::clamp(new_value, total);
    // }

    // pub fn attack(&mut self, new_value: i32) {
    //     let total =
    //         self.max_hp + self.defense + self.special_attack + self.special_defense + self.speed;
    //     self.attack = EffortValues::clamp(new_value, total);
    // }

    // pub fn defense(&mut self, new_value: i32) {
    //     let total =
    //         self.max_hp + self.attack + self.special_attack + self.special_defense + self.speed;
    //     self.defense = EffortValues::clamp(new_value, total);
    // }

    // pub fn special_attack(&mut self, new_value: i32) {
    //     let total = self.max_hp + self.attack + self.defense + self.special_defense + self.speed;
    //     self.special_attack = EffortValues::clamp(new_value, total);
    // }

    // pub fn special_defense(&mut self, new_value: i32) {
    //     let total = self.max_hp + self.attack + self.defense + self.special_attack + self.speed;
    //     self.special_defense = EffortValues::clamp(new_value, total);
    // }

    // pub fn speed(&mut self, new_value: i32) {
    //     let total =
    //         self.max_hp + self.attack + self.defense + self.special_attack + self.special_defense;
    //     self.speed = EffortValues::clamp(new_value, total);
    // }

    fn clamp(value: i32, total: i32) -> i32 {
        cmp::min(MAX_EV, cmp::min(MAX_TOTAL_EV - total, value))
    }
}
