/// Represents one of the 17 Pokemon Types
#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum PokemonType {
    Normal,
    Grass,
    Fire,
    Electric,
    Water,
    Fighting,
    Psychic,
    Ghost,
    Bug,
    Flying,
    Rock,
    Ground,
    Steel,
    Ice,
    Fairy,
    Dark,
    Poison,
    Dragon,
}

/// Helper function that accepts a string and returns the correct PokemonType
pub fn get_type_by_string(type_name: String) -> PokemonType {
    match type_name.as_ref() {
        "Normal" => PokemonType::Normal,
        "Grass" => PokemonType::Grass,
        "Fire" => PokemonType::Fire,
        "Electric" => PokemonType::Electric,
        "Water" => PokemonType::Water,
        "Fighting" => PokemonType::Fighting,
        "Psychic" => PokemonType::Psychic,
        "Ghost" => PokemonType::Ghost,
        "Bug" => PokemonType::Bug,
        "Flying" => PokemonType::Flying,
        "Rock" => PokemonType::Rock,
        "Ground" => PokemonType::Ground,
        "Steel" => PokemonType::Steel,
        "Ice" => PokemonType::Ice,
        "Fairy" => PokemonType::Fairy,
        "Dark" => PokemonType::Dark,
        "Poison" => PokemonType::Poison,
        "Dragon" => PokemonType::Dragon,
        _ => unreachable!(),
    }
}

/// Helper function that accepts an `i32` and returns the corresponding PokemonType
/// @see_also types.csv in the data crate
pub fn get_type_by_id(type_id: i32) -> PokemonType {
    match type_id {
        1 => PokemonType::Normal,
        12 => PokemonType::Grass,
        10 => PokemonType::Fire,
        13 => PokemonType::Electric,
        11 => PokemonType::Water,
        2 => PokemonType::Fighting,
        14 => PokemonType::Psychic,
        8 => PokemonType::Ghost,
        7 => PokemonType::Bug,
        3 => PokemonType::Flying,
        6 => PokemonType::Rock,
        5 => PokemonType::Ground,
        9 => PokemonType::Steel,
        15 => PokemonType::Ice,
        18 => PokemonType::Fairy,
        17 => PokemonType::Dark,
        4 => PokemonType::Poison,
        16 => PokemonType::Dragon,
        _ => unreachable!(),
    }
}
