use std::cmp::{max, min};
use std::ops;

#[derive(Debug, Clone, Serialize)]
pub struct HitPoints {
    /// Max HP: Should only ever change when a Pokemon's level/stats change
    maximum: i32,
    /// Current HP
    /// Affected negatively by battle damage, poison, etc.
    /// Affected positively by healing techniques, potions, etc.
    current: i32,
}

impl HitPoints {
    /// Set max hp and current hp to a given number
    /// Returns a struct with the new values
    pub fn initial(starting_hp: i32) -> HitPoints {
        HitPoints {
            maximum: starting_hp,
            current: starting_hp,
        }
    }

    /// Return maximum HP
    pub fn maximum(&self) -> i32 {
        self.maximum
    }

    /// Increase the maximum hp and calculate current HP based on the change
    /// Returns a struct with the new values
    pub fn set_maximum(&self, maximum: i32) -> HitPoints {
        if maximum < 0 {
            panic!("Hit points cannot be negative.");
        }

        HitPoints {
            current: max(0, self.current + (maximum - self.maximum)),
            maximum,
        }
    }

    /// Return current HP
    pub fn current(&self) -> i32 {
        self.current
    }
}

/// Allow == comparisons on the HP structure
/// E.G. `pokemon.hp == 10` will check to see if current HP is 10
impl PartialEq<i32> for HitPoints {
    fn eq(&self, other: &i32) -> bool {
        &self.current == other
    }
}

/// Allow + operations on the HP structure
/// E.G. `pokemon.hp = pokemon.hp + 5` will increase current HP by 5
impl ops::Add<i32> for HitPoints {
    type Output = HitPoints;

    fn add(self, other: i32) -> HitPoints {
        HitPoints {
            current: max(0, min(self.maximum, self.current + other)),
            maximum: self.maximum,
        }
    }
}

/// Allow += binary assignment
/// E.G. `pokemon.hp += 10` will increase current HP by 10
impl ops::AddAssign<i32> for HitPoints {
    fn add_assign(&mut self, other: i32) {
        *self = HitPoints {
            current: max(0, min(self.maximum, self.current + other)),
            maximum: self.maximum,
        }
    }
}

/// Allow + operations on the HP structure
/// E.G. `pokemon.hp = pokemon.hp + 5` will decrease current HP by 5
impl ops::Sub<i32> for HitPoints {
    type Output = HitPoints;

    fn sub(self, other: i32) -> HitPoints {
        HitPoints {
            current: max(0, min(self.maximum, self.current - other)),
            maximum: self.maximum,
        }
    }
}

/// Allow += binary assignment
/// E.G. `pokemon.hp -= 10` will decrease current HP by 10
impl ops::SubAssign<i32> for HitPoints {
    fn sub_assign(&mut self, other: i32) {
        *self = HitPoints {
            current: max(0, min(self.maximum, self.current - other)),
            maximum: self.maximum,
        }
    }
}
