extern crate rand;
use self::rand::{thread_rng, Rng};

use stat::Stat;

#[derive(Debug, Clone, Serialize)]
pub enum Natures {
    Hardy,
    Lonely,
    Brave,
    Adamant,
    Naughty,
    Bold,
    Docile,
    Relaxed,
    Impish,
    Lax,
    Timid,
    Hasty,
    Serious,
    Jolly,
    Naive,
    Modest,
    Mild,
    Quiet,
    Bashful,
    Rash,
    Calm,
    Gentle,
    Sassy,
    Careful,
    Quirky,
}

pub fn random() -> Nature {
    match rand::thread_rng().gen_range(0, 25) {
        0 => Natures::Hardy.value(),
        1 => Natures::Lonely.value(),
        2 => Natures::Brave.value(),
        3 => Natures::Adamant.value(),
        4 => Natures::Naughty.value(),
        5 => Natures::Bold.value(),
        6 => Natures::Docile.value(),
        7 => Natures::Relaxed.value(),
        8 => Natures::Impish.value(),
        9 => Natures::Lax.value(),
        10 => Natures::Timid.value(),
        11 => Natures::Hasty.value(),
        12 => Natures::Serious.value(),
        13 => Natures::Jolly.value(),
        14 => Natures::Naive.value(),
        15 => Natures::Modest.value(),
        16 => Natures::Mild.value(),
        17 => Natures::Quiet.value(),
        18 => Natures::Bashful.value(),
        19 => Natures::Rash.value(),
        20 => Natures::Calm.value(),
        21 => Natures::Gentle.value(),
        22 => Natures::Sassy.value(),
        23 => Natures::Careful.value(),
        24 => Natures::Quirky.value(),
        _ => unreachable!(),
    }
}

impl Natures {
    pub fn value(&self) -> Nature {
        use self::Natures::*;

        match *self {
            Hardy => Nature {
                increased_attr: Stat::Attack,
                decreased_attr: Stat::Attack,
            },
            Lonely => Nature {
                increased_attr: Stat::Attack,
                decreased_attr: Stat::Defense,
            },
            Brave => Nature {
                increased_attr: Stat::Attack,
                decreased_attr: Stat::Speed,
            },
            Adamant => Nature {
                increased_attr: Stat::Attack,
                decreased_attr: Stat::SpecialAttack,
            },
            Naughty => Nature {
                increased_attr: Stat::Attack,
                decreased_attr: Stat::SpecialDefense,
            },
            Bold => Nature {
                increased_attr: Stat::Defense,
                decreased_attr: Stat::Attack,
            },
            Docile => Nature {
                increased_attr: Stat::Defense,
                decreased_attr: Stat::Defense,
            },
            Relaxed => Nature {
                increased_attr: Stat::Defense,
                decreased_attr: Stat::Speed,
            },
            Impish => Nature {
                increased_attr: Stat::Defense,
                decreased_attr: Stat::SpecialAttack,
            },
            Lax => Nature {
                increased_attr: Stat::Defense,
                decreased_attr: Stat::SpecialDefense,
            },
            Timid => Nature {
                increased_attr: Stat::Speed,
                decreased_attr: Stat::Attack,
            },
            Hasty => Nature {
                increased_attr: Stat::Speed,
                decreased_attr: Stat::Defense,
            },
            Serious => Nature {
                increased_attr: Stat::Speed,
                decreased_attr: Stat::Speed,
            },
            Jolly => Nature {
                increased_attr: Stat::Speed,
                decreased_attr: Stat::SpecialAttack,
            },
            Naive => Nature {
                increased_attr: Stat::Speed,
                decreased_attr: Stat::SpecialDefense,
            },
            Modest => Nature {
                increased_attr: Stat::SpecialAttack,
                decreased_attr: Stat::Attack,
            },
            Mild => Nature {
                increased_attr: Stat::SpecialAttack,
                decreased_attr: Stat::Defense,
            },
            Quiet => Nature {
                increased_attr: Stat::SpecialAttack,
                decreased_attr: Stat::Speed,
            },
            Bashful => Nature {
                increased_attr: Stat::SpecialAttack,
                decreased_attr: Stat::SpecialAttack,
            },
            Rash => Nature {
                increased_attr: Stat::SpecialAttack,
                decreased_attr: Stat::SpecialDefense,
            },
            Calm => Nature {
                increased_attr: Stat::SpecialDefense,
                decreased_attr: Stat::Attack,
            },
            Gentle => Nature {
                increased_attr: Stat::SpecialDefense,
                decreased_attr: Stat::Defense,
            },
            Sassy => Nature {
                increased_attr: Stat::SpecialDefense,
                decreased_attr: Stat::Speed,
            },
            Careful => Nature {
                increased_attr: Stat::SpecialDefense,
                decreased_attr: Stat::SpecialAttack,
            },
            Quirky => Nature {
                increased_attr: Stat::SpecialDefense,
                decreased_attr: Stat::SpecialDefense,
            },
        }
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct Nature {
    pub increased_attr: Stat,
    pub decreased_attr: Stat,
}

impl Nature {
    pub fn is_neutral(&self) -> bool {
        self.increased_attr == self.decreased_attr
    }
}
